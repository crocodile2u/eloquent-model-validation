<?php

namespace Tests;

use EloquentModelValidation\HasValidation;
use EloquentModelValidation\ModelValidationFailed;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use PHPUnit\Framework\TestCase;

class HasValidationTest extends TestCase
{
    public function testValidator()
    {
        ValidatorFacade::swap($this->factoryMock(false));
        $model = new class extends Model {
            use HasValidation;
        };
        $this->assertInstanceOf(Validator::class, $model->validator());
    }

    public function testSaveValidated()
    {
        $this->expectException(ModelValidationFailed::class);
        ValidatorFacade::swap($this->factoryMock(true));
        $model = new class extends Model {
            use HasValidation;
        };
        $model->saveValidated();
    }

    public function tearDown(): void
    {
        ValidatorFacade::clearResolvedInstance('validator');
    }

    private function factoryMock(bool $fails)
    {
        $validator = new class($fails) implements Validator {
            private bool $fails;
            public function __construct(bool $fails)
            {
                $this->fails = $fails;
            }
            public function validate() {}
            public function validated() {}
            public function fails()
            {
                return $this->fails;
            }
            public function failed() {}
            public function sometimes($attribute, $rules, callable $callback) {}
            public function after($callback) {}
            public function errors()
            {
                return new class implements MessageBag {
                    public function toArray() {}
                    public function keys() {}
                    public function add($key, $message) {}
                    public function merge($messages) {}
                    public function has($key) {}
                    public function first($key = null, $format = null) {}
                    public function get($key, $format = null) {}
                    public function all($format = null) {}
                    public function getMessages() {}
                    public function getFormat() {}
                    public function setFormat($format = ':message') {}
                    public function isEmpty() {}
                    public function isNotEmpty() {}
                    public function count() {}
                };
            }
            public function getMessageBag() {}
        };

        $factory = $this->getMockBuilder(Factory::class)->getMock();
        $factory->method('make')->willReturn($validator);
        return $factory;
    }
}
