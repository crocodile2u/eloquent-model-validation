<?php

declare(strict_types=1);

namespace EloquentModelValidation;

use Illuminate\Contracts\Support\MessageBag;

class ModelValidationFailed extends \RuntimeException
{
    /**
     * @var MessageBag
     */
    private MessageBag $errors;

    public function __construct(MessageBag $errors)
    {
        parent::__construct("Model validation rules failed");
        $this->errors = $errors;
    }

    /**
     * @return MessageBag
     */
    public function getErrors(): MessageBag
    {
        return $this->errors;
    }
}