<?php
declare(strict_types=1);

namespace EloquentModelValidation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

trait HasValidation
{
    public function rules(): array
    {
        return [];
    }

    public function validator(): \Illuminate\Contracts\Validation\Validator
    {
        /** @var Model|self $this */
        $validated = clone $this;
        return Validator::make(
            $validated->makeVisible($this->hidden)->toArray(),
            $validated->rules()
        );
    }

    public function saveValidated(array $options = []): bool
    {
        /** @var Model|self $this */
        $validator = $this->validator();
        if ($validator->fails()) {
            throw new ModelValidationFailed($validator->errors());
        }

        return $this->save($options);
    }
}